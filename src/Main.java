import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

public class Main {
    static int countStop = 0;
    static int countCharSMessage = 0;
    static int countBiteSChar = 0;
    static int dataInBinary = 0;
    static int dataOutBinary = 0;
    static boolean skipStatus;
    static boolean flag;
    static boolean theEnd;

    public static void main(String[] arg) throws IOException {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Меню:\n 1) Закодировать сообщение\n 2) Расшифровать сообщение\n 3) Выйти\nВыберите что нужно сделать: ");
            switch (sc.nextInt()) {
                case (1):
                    System.out.print("Введите сообщение:");
                    sc.nextLine();
//                    stegocoder(sc.nextLine(),"Test.bmp","Out.bmp");
                    stegocoder(sc.nextLine(), "mars.bmp", "Out.bmp");
                    break;
                case (2):
                    System.out.println("Начат процесс декодирования сообщения!");
                    decoder("Out.bmp");
                    break;
                case (3):
                    System.exit(0);
                    break;
            }
        }
    }

    public static void decoder(String codeBmp) throws IOException {
        int count = 0;
        FileInputStream codeStream = new FileInputStream(codeBmp);
        int countSkip = 0;
        StringBuilder strBuilder = new StringBuilder();
        while ((dataInBinary = codeStream.read()) != -1) {
            if (countSkip < 54) {
                countSkip++;
            } else {
                char[] colorCharArray = Integer.toBinaryString(dataInBinary).toCharArray();
                strBuilder.append(colorCharArray[colorCharArray.length - 1]);
                count++;
                if (count == 8) {
                    if ((char) Integer.parseInt(strBuilder.toString(), 2) == 127) {
                        System.out.println();
                        return;
                    }
                    System.out.print((char) Integer.parseInt(strBuilder.toString(), 2));
                    count = 0;
                    strBuilder = new StringBuilder();
                }
            }
        }
        System.out.println();
    }

    public static void stegocoder(String sMessage, String inBmp, String outBmp) throws IOException {
        int countSkip = 0;
        skipStatus = false;
        countBiteSChar = 0;
        countCharSMessage = 0;
        FileInputStream inputStream = new FileInputStream(inBmp);
        OutputStream outStream = new FileOutputStream(outBmp);
        char[] sMessageCharArray = sMessage.toCharArray();
        while ((dataInBinary = inputStream.read()) != -1) {
            if (countSkip < 54) {
                outStream.write(dataInBinary);
                countSkip++;
            } else if (!skipStatus) {
                char[] colorCharArray = Integer.toBinaryString(dataInBinary).toCharArray();
                colorCharArray[colorCharArray.length - 1] = Character.forDigit(charToBinary(sMessageCharArray), 10);
                StringBuilder strBuilder = new StringBuilder();
                for (char c : colorCharArray) {
                    strBuilder.append(c);
                }
                dataOutBinary = Integer.parseInt(strBuilder.toString(), 2);
                outStream.write(dataOutBinary);
                System.out.println("data in=" + dataInBinary + "data out=" + dataOutBinary);
            } else {
                if (!theEnd) {
                    char[] colorCharArray = Integer.toBinaryString(dataInBinary).toCharArray();
                    if (countStop == 0) {
                        colorCharArray[colorCharArray.length - 1] = '0';
                    } else colorCharArray[colorCharArray.length - 1] = '1';
                    StringBuilder strBuilder = new StringBuilder();
                    for (char c : colorCharArray) {
                        strBuilder.append(c);
                    }
                    dataOutBinary = Integer.parseInt(strBuilder.toString(), 2);
                    outStream.write(dataOutBinary);
                    countStop++;
                    if (countStop == 8) theEnd = false;
                } else outStream.write(dataInBinary);
            }
        }
    }

    public static int charToBinary(char[] sMessageCharArray) {
        char[] biteCharArray = Integer.toBinaryString(sMessageCharArray[countCharSMessage]).toCharArray();
        if (biteCharArray.length != 8 && !flag) {
            countBiteSChar = 0;
            flag = true;
            countBiteSChar -= 8 - biteCharArray.length;
        }
        if (countBiteSChar < 0) {
            countBiteSChar++;
            return 0;
        }
        countBiteSChar++;
        if (countBiteSChar >= biteCharArray.length) {
            countCharSMessage++;
            flag = false;
        }
        if (countCharSMessage >= sMessageCharArray.length) {
            skipStatus = true;
        }
        return Character.getNumericValue(biteCharArray[countBiteSChar - 1]);
    }
}
